package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Model.Grad;


public class GradDAO {

	

	public static Grad getGradByNaziv(Connection conn, String naziv) {
		Grad grad = null;
		try {
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt
					.executeQuery("SELECT ptt " +
							"FROM gradovi WHERE naziv = " 
							+ naziv);

			if (rset.next()) {
				
				int ptt = rset.getInt(1);
			
				grad = new Grad(ptt, naziv);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return grad;
	}
	
	
	public static List<Grad> getAll(Connection conn) {
		List<Grad> retVal = new ArrayList<Grad>();
		try {
			String query = "SELECT * FROM gradovi ";
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt.executeQuery(query.toString());
			while (rset.next()) {
				int ptt = rset.getInt(1);
				
				String naziv = rset.getString(2);
					
				Grad grad = new Grad(ptt, naziv);
			
				retVal.add(grad);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retVal;
	}

	public static boolean add(Connection conn, Grad grad){
		boolean retVal = false;
		try {
			String update = "INSERT INTO gradovi (naziv) values (?, ?)";
			PreparedStatement pstmt = conn.prepareStatement(update);
			
			pstmt.setString(1, grad.getNaziv());
			

			if(pstmt.executeUpdate() == 1){
				retVal = true;
			}
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}
	
	public static boolean update(Connection conn, Grad grad) {
		boolean retVal = false;
		try {
			String update = "UPDATE gradovi SET naziv=? WHERE ptt=?";
			PreparedStatement pstmt = conn.prepareStatement(update);
			
			pstmt.setString(1, grad.getNaziv());
			pstmt.setInt(2, grad.getPtt());
			if(pstmt.executeUpdate() == 1)
				retVal = true;
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}
	
	public static boolean delete(Connection conn, int ptt) {
		boolean retVal = false;
		try {
			String update = "DELETE FROM gradovi WHERE " +
					"grad_ptt = " + ptt;
			Statement stmt = conn.createStatement();
			if (stmt.executeUpdate(update) == 1)
				retVal = true;
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}

	public static Grad pronadjiGradPtt(Connection conn, int ptt){
	Grad grad = null;
	String query = "Select * from gradovi where ptt = " + ptt;
	try {
		Statement stmt = conn.createStatement();
		ResultSet rset = stmt.executeQuery(query);
		while(rset.next()){
			String ime = rset.getString(1);
			
			grad = new Grad(ptt, ime);
			
		}
		rset.close();
		stmt.close();
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	return grad;
	
	
}
	}

	





