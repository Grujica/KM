package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import DAO.GradDAO;
import Model.Grad;
import Model.Manifestacija;

public class ManifestacijaDAO {

	
	public static Manifestacija PronadjiManifestacijuPoId(Connection conn, int id) {
		Manifestacija manifestacija = null;
		try {
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt
					.executeQuery("SELECT naziv, brPosetilaca, ptt_grada " +
							"FROM manifestacije WHERE id = " 
							+ id);

			if (rset.next()) {
				
				String naziv = rset.getString(1);
				int brPosetilaca = rset.getInt(2);
				
				int ptt = rset.getInt(3);
				Grad grad = GradDAO.pronadjiGradPtt(conn, ptt);
				manifestacija = new Manifestacija(id, naziv, brPosetilaca, grad);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return manifestacija;
	}
		public static List<Manifestacija> prikazSvihManifestacija(Connection conn){
			
			List<Manifestacija> manifestacije = new ArrayList<Manifestacija>();
			
			String query = " Select id, naziv, brPosetilaca,g.ptt, g.ime from manifestacije m"
					+ " left join gradovi g on m.ptt_grada = g.ptt";
			
			try {
				Statement stmt = conn.createStatement();
				ResultSet rset = stmt.executeQuery(query);
				while(rset.next()){
					
					int id = rset.getInt(1);
					String naziv = rset.getString(2);
					int brPosetilaca = rset.getInt(3);
					int ptt = rset.getInt(4);
					String ime =rset.getString(5);
					
					Grad grad = new Grad(ptt, ime);
					Manifestacija km = new Manifestacija(id, naziv, brPosetilaca, grad);
					
					manifestacije.add(km);
					
					
					
				}
				rset.close();
				stmt.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			
			return manifestacije;
						
			
		}

		public static boolean  izmeniNazivManifestacije(Connection conn, Manifestacija km){
			
			boolean uspesno = false;
			
			try {
				conn.setAutoCommit(false);
				String query  = "Update manifestacije set naziv  = ? where id = ?";
				PreparedStatement pstmt = conn.prepareStatement(query);
				pstmt.setString(1, km.getNaziv());
				pstmt.setInt(2, km.getId());
				if (pstmt.executeUpdate() == 1){
					uspesno = true;
				}
				pstmt.close();
				conn.commit();
				if(conn != null){
					conn.rollback();
				}
				conn.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			return uspesno;				
		}
		
		public static boolean dodajNovuManifestaciju(Connection conn, Manifestacija km){
			
			boolean uspesno = false;
			
			String query = "Insert into manifestacije (naziv, brPosetilaca) values (?,?)";
			try {
				PreparedStatement psmt = conn.prepareStatement(query);
				psmt.setString(1, km.getNaziv());
				psmt.setInt(2, km.getBrPosetilaca());
				
				if(psmt.executeUpdate() == 1){
					uspesno = true;
				}
				psmt.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			return uspesno;
		}
		
		public static boolean izmeniPodatkeOManifestaciji(Connection conn, Manifestacija km){
			
			boolean uspesno  = false;
			
			try {
				conn.setAutoCommit(false);
				String query = "Update manifestacije set naziv =?, brPosetilaca =? where id = ?";
				PreparedStatement pstmt = conn.prepareStatement(query);
				pstmt.setString(1, km.getNaziv());
				pstmt.setInt(2, km.getBrPosetilaca());
				
				pstmt.setInt(3, km.getId());
				if(pstmt.executeUpdate() == 1){
					uspesno = true;
				}
				pstmt.close();
				conn.commit();
				if(conn !=null){
					conn.rollback();
				}
				conn.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			
			return uspesno;		
		}
		
		public static boolean brisanjeManifestacije(Connection conn, int id){
			
			boolean uspesno = false;
			String query = "Delete from manifestacije where id= " + id;
			try {
				Statement stmt = conn.createStatement();
				if(stmt.executeUpdate(query)==1){
					uspesno = true;
				}
				stmt.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			return uspesno;
			
		}
		public static String izvestajOpPosetiocima(Connection conn){
			
			String izvestaj = "";
			String query = "Select naziv,brPosetilaca from manifestacije "
					+ "where brPosetilaca = (select max(brPosetilaca) from manifestacije)";
			
			try {
				Statement stmt = conn.createStatement();
				ResultSet rset = stmt.executeQuery(query);
				while(rset.next()){
					String naziv = rset.getString(1);
					int max = rset.getInt(2);
					
					izvestaj = "<" + naziv + ">" + "<" + max + ">";
					
				}
				rset.close();
				stmt.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			return izvestaj;
					
		}
}	




